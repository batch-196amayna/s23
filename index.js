console.log("Hello World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Balbasaur"],
	friends: ["Brock", "Misty"],
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you!");
	}
	}
console.log(trainer);
console.log("Result of dot notation");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer.pokemon);
console.log("Result of talk method");
trainer.talk();

function Pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = level*3;
	this.attack = level*1.5;
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
	}
	this.faint = function(){
		console.log(this.name + " has fainted!");
	}
}
let pokemon1 = new Pokemon("Pikachu", 12);
console.log(pokemon1);
let pokemon2 = new Pokemon("Geodude", 8);
console.log(pokemon2);
let pokemon3 = new Pokemon("Mewtwo", 100);
console.log(pokemon3);


pokemon1.tackle(pokemon2);
let healthPower = pokemon1.health - pokemon2.attack
if(pokemon1.health - pokemon2.attack !== 0){
	console.log(pokemon1.name + "'s health is now " + healthPower);
} else {
	pokemon1.faint(pokemon2);
}


	